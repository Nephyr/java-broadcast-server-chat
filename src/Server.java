
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Iterator;

public class Server {

    private final int PORT = 8000;
    private HashSet<String> names;
    private HashSet<PrintWriter> writers;

    public static void main(String[] args) {
        try {
            System.out.println("\nBenvenuto in \"Broadcast Chat - Server\"");
            System.out.println("Produttore: Vanzo Luca Samuele, (c) 2013\n");

            Server istance = new Server();
            istance.Run();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public Server() {
        this.names = new HashSet<>();
        this.writers = new HashSet<>();
    }

    public void Run() throws Exception {
        ServerSocket listener = new ServerSocket(PORT);
        System.out.println("Server> Avviato su porta " + PORT);

        try {
            while (true) {
                new Handler(listener.accept()).start();
            }
        } finally {
            listener.close();
        }
    }

    private class Handler extends Thread {

        private String name;
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                writers.add(out);
                out.println("USERNAME");
                name = in.readLine();

                if (name != null) {
                    synchronized (names) {
                        if (!names.contains(name)) {
                            names.add(name);

                            String tmp = "";
                            for (String it : names) {
                                tmp += (("".equals(tmp)) ? "" : "::") + it;
                            }

                            for (PrintWriter writer : writers) {
                                writer.println("LUSERS " + tmp);
                            }
                        } else {
                            out.println("INVALIDUSER");
                        }
                    }

                    if (names.contains(name)) {
                        boolean checkRun = true;
                        boolean checkFirst = true;

                        out.println("VALIDUSER");

                        while (checkRun) {
                            String input = (checkFirst) ? "Mi sono connesso! CIAO!" : in.readLine();

                            if (input == null || input.equals("bye")) {
                                checkRun = false;
                                input = "Mi sono disconnesso dal server! BYE!";
                            }

                            for (PrintWriter writer : writers) {
                                writer.println("MESSAGE " + name.toUpperCase() + "> " + input);
                            }

                            if (checkFirst) {
                                checkFirst = false;
                            }
                        }

                        out.println("DISCONNECT");
                    }
                }

                socket.close();
            } catch (Exception e) {
                System.out.println("Server> Disconnessione forzata dal client!");
            }

            if (name != null) {
                names.remove(name);
            }

            if (out != null) {
                writers.remove(out);
            }

            String tmp = "";
            for (String it : names) {
                tmp += (("".equals(tmp)) ? "" : "::") + it;
            }

            for (PrintWriter writer : writers) {
                writer.println("LUSERS " + tmp);
            }
        }
    }
}
